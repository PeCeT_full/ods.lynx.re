<?php

/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the MIT license. For more information, see
 * <http://www.doctrine-project.org>.
 */
 
/**
 * ods.lynx.re - Unofficial Odsluchane.eu API
 * @version 0.2.0.1
 * @file radiolst.php (get radio station list)
 *
 * @license http://www.opensource.org/licenses/mit-license.html  MIT License
 * @author PeCeT_full <me@pecetfull.pl>
 */

error_reporting(0);

// FUNCTIONS
function getHtmlCode()
{
    $htmlCode = '';
    
    $url = 'https://www.odsluchane.eu/';
    $htmlCode = file_get_contents($url);
    
    return $htmlCode;
}

function retrievePageTitle($htmlCode)
{
    $pageTitle = '';
    
    $pageTitleBegin = '<title>';
    $pageTitleEnd = '</title>';
    
    $indexOfPageTitleBegin = stripos($htmlCode, $pageTitleBegin);
    $indexOfPageTitleEnd = stripos($htmlCode, $pageTitleEnd);
    if ($indexOfPageTitleBegin !== false && $indexOfPageTitleEnd !== false)
    {
        $pageTitle = substr(substr($htmlCode, 0, $indexOfPageTitleEnd), $indexOfPageTitleBegin + strlen($pageTitleBegin));
    }
    
    return $pageTitle;
}

function retrieveRadioList($htmlCode)
{
    $radioList = '';
    
    $stationsDefaultPos = stripos($htmlCode, ':stations-default="');
    if ($stationsDefaultPos)
    {
        $radioList = substr($htmlCode, $stationsDefaultPos + 19);
        $radioList = substr($radioList, 0, stripos($radioList, '"'));
        if (!empty($radioList))
        {
            $radioList = str_replace('&quot;', '"', $radioList);
            $radioList = json_decode($radioList);
        }
    }
    
    return $radioList;
}

function returnApiResponse($htmlCode)
{
    $response = '';
    
    $pageTitle = retrievePageTitle($htmlCode);
    if (stripos($pageTitle, 'odSluchane.eu'))
    {
        $radioList = retrieveRadioList($htmlCode);
        
        $success = !empty($radioList);
        
        $response = json_encode(array(
            'success' => $success,
            'summary' => $success ? $radioList : 'Unknown error while retrieving radio station list.'
        ));
    }
    else
    {
        $response = json_encode(array(
            'success' => false,
            'summary' => 'Service unavailable.'
        ));
    }
    
    return $response;
}

// EXECUTION
header('Content-Type: application/json');

$htmlCode = getHtmlCode();

$response = returnApiResponse($htmlCode);
echo $response;

?>