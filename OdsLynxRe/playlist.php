<?php

/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the MIT license. For more information, see
 * <http://www.doctrine-project.org>.
 */
 
/**
 * ods.lynx.re - Unofficial Odsluchane.eu API
 * @version 0.2.0.1
 * @file playlist.php (get track list)
 *
 * @license http://www.opensource.org/licenses/mit-license.html  MIT License
 * @author PeCeT_full <me@pecetfull.pl>
 */

error_reporting(0);

// FUNCTIONS
function getHtmlCode($r)
{
    $htmlCode = '';
    
    $date = $_GET['date'];
    $timeFrom = $_GET['time_from'];
    $timeTo = $_GET['time_to'];
    
    $url = "https://www.odsluchane.eu/szukaj.php?r={$r}";
    if (!empty($date) && ($timeFrom === '0' || !empty($timeFrom)) && ($timeTo === '0' || !empty($timeTo)))
    {
        $url = "{$url}&date={$date}&time_from={$timeFrom}&time_to={$timeTo}";
    }
    $htmlCode = file_get_contents($url);
    
    return $htmlCode;
}

function retrievePageTitle($htmlCode)
{
    $pageTitle = '';
    
    $pageTitleBegin = '<title>';
    $pageTitleEnd = '</title>';
    
    $indexOfPageTitleBegin = stripos($htmlCode, $pageTitleBegin);
    $indexOfPageTitleEnd = stripos($htmlCode, $pageTitleEnd);
    if ($indexOfPageTitleBegin !== false && $indexOfPageTitleEnd !== false)
    {
        $pageTitle = substr(substr($htmlCode, 0, $indexOfPageTitleEnd), $indexOfPageTitleBegin + strlen($pageTitleBegin));
    }
    
    return $pageTitle;
}

function retrieveListDate($htmlCode)
{
    $listDate = '';
    
    $dateDefaultPos = stripos($htmlCode, 'date-default="');
    if ($dateDefaultPos)
    {
        $listDate = substr($htmlCode, $dateDefaultPos + 14);
        $listDate = substr($listDate, 0, stripos($listDate, '"'));
    }
    
    return $listDate;
}

function retrieveListTimeFrom($htmlCode)
{
    $listTimeFrom = '';
    
    $timeDefaultPos = stripos($htmlCode, 'time-default="');
    if ($timeDefaultPos)
    {
        $listTimeFrom = substr($htmlCode, $timeDefaultPos + 14);
        $listTimeFrom = substr($listTimeFrom, 0, stripos($listTimeFrom, '"'));
    }
    
    return $listTimeFrom;
}

function populatePlaylist($htmlCode)
{
    $playlist = array();
    
    $trBegin = '<tr>';
    $tdBegin = '<td>';
    $aClass = 'class="title-link">';
    $aEnd = '</a>';
    while ($htmlCode !== '')
    {
        $indexOfAClass = stripos($htmlCode, $aClass);
        if ($indexOfAClass !== false)
        {
            $parsedHtml = substr($htmlCode, 0, $indexOfAClass);
            $indexOfTrBegin = strripos($parsedHtml, $trBegin);
            if ($indexOfTrBegin !== false)
            {
                $parsedHtml = substr($htmlCode, $indexOfTrBegin + strlen($trBegin));
                $indexOfTdBegin = stripos($parsedHtml, $tdBegin);
                if ($indexOfTrBegin !== false)
                {
                    $parsedHtml = substr($parsedHtml, $indexOfTdBegin + strlen($tdBegin));
                    $time = substr($parsedHtml, 0, 5);
                    $indexOfAClass = stripos($parsedHtml, $aClass);
                    $parsedHtml = substr($parsedHtml, $indexOfAClass + strlen($aClass));
                    $indexOfAEnd = stripos($parsedHtml, $aEnd);
                    $title = trim(substr($parsedHtml, 0, $indexOfAEnd));
                    $title = htmlspecialchars_decode($title, ENT_QUOTES);
                    $htmlCode = substr($parsedHtml, $indexOfAEnd + strlen($aEnd));
                    
                    $record = array('time' => $time, 'title' => $title);
                    array_push($playlist, $record);
                }
                else
                {
                    $htmlCode = '';
                }
            }
            else
            {
                $htmlCode = '';
            }
        }
        else
        {
            $htmlCode = '';
        }
    }
    
    return $playlist;
}

function returnApiResponse($r)
{
    $response = '';
    
    if (!empty($r))
    {
        $htmlCode = getHtmlCode($r);
        
        $pageTitle = retrievePageTitle($htmlCode);
        
        if (stripos($pageTitle, 'Playlista'))
        {
            $radioName = substr($pageTitle, 0, stripos($pageTitle, html_entity_decode('&mdash;')) - 1);
            $listDate = retrieveListDate($htmlCode);
            $listTimeFrom = retrieveListTimeFrom($htmlCode);
            $playlist = populatePlaylist($htmlCode);
            
            $response = json_encode(array(
                'success' => true,
                'name' => $radioName,
                'date' => $listDate,
                'timeFrom' => $listTimeFrom,
                'summary' => $playlist
            ));
        }
        else
        {
            $response = json_encode(array(
                'success' => false,
                'summary' => stripos($pageTitle, 'odSluchane.eu') ? 'Radio with the specified ID unavailable.' : 'Service unavailable.'
            ));
        }
    }
    else
    {
        $response = json_encode(array(
            'success' => false,
            'summary' => 'Radio ID unspecified.'
        ));
    }
    
    return $response;
}

// EXECUTION
header('Content-Type: application/json');

$r = $_GET['r'];

$response = returnApiResponse($r);
echo $response;

?>