# ods.lynx.re � Unofficial Odsluchane.eu API

## Get track list (playlist.php)

### Usage (GET parameters)

URL: [http(s)://ods.lynx.re/playlist.php?r=1&date=01-01-1000&time_from=0&time_to=2](https://ods.lynx.re/playlist.php?r=1&date=01-01-1000&time_from=0&time_to=2)

* **r** � radio station ID. This is the only mandatory parameter.
* **date** � valid input date format: dd-mm-yyyy.
* **time_from** � integer, valid values: 0-23.
* **time_to** � integer, valid values: 0-23. To set time_to to 24 (as in 24:00), type 0.

#### Notes

If any of the values of: **date**, **time_from** or **time_to** appears wrong, a playlist from the last 2-hour range of today will be returned.

The difference between **time_from** and **time_to** cannot exceed 10, otherwise the output playlist will be empty!

### Result (JSON body)

* **success** � boolean (*true*/*false*).
* **name** � radio station name, appears only when **success** is *true*.
* **date** � date the list relates to, output format: dd-mm-yyyy, appears only when **success** is *true*.
* **timeFrom** � beginning time the list relates to, output format: hh:mm, appears only when **success** is *true*.
* **summary** � if **success** is *true*, a playlist array is returned, otherwise an error message.

#### Playlist item body

* **time** � time format: hh:mm.
* **title** � track name.

#### Possible error messages

* "Radio ID unspecified."
* "Service unavailable."
* "Radio with the specified ID unavailable." (can also appear on site maintenance)

## Get radio station list (radiolst.php)

### Usage (GET parameters)

URL: [http(s)://ods.lynx.re/radiolst.php](https://ods.lynx.re/radiolst.php)

### Result (JSON body)

* **success** � boolean (*true*/*false*).
* **summary** � if success is *true*, a station list array is returned, otherwise an error message.

#### Station list body

The list consists of a few radio station groups with each of them containing **groupName** and another array called **stations**.

Each item of **stations** contains **id** and **name**.

#### Possible error messages

* "Service unavailable."
* "Unknown error while retrieving radio station list."

## API licence

The MIT License (MIT)

Copyright (c) 2020 PeCeT_full

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.